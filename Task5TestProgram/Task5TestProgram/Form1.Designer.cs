﻿namespace Task5TestProgram
{
    partial class FormOperatorsTester
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textEqualImagesResult = new System.Windows.Forms.TextBox();
            this.labelEqualImagesResult = new System.Windows.Forms.Label();
            this.labelDiffResultTitle = new System.Windows.Forms.Label();
            this.labelOurResultTitle = new System.Windows.Forms.Label();
            this.labelOpencvResultTitle = new System.Windows.Forms.Label();
            this.labelOriginalImageTitle = new System.Windows.Forms.Label();
            this.OwnPictureBox = new System.Windows.Forms.PictureBox();
            this.OpenCVPictureBox = new System.Windows.Forms.PictureBox();
            this.DifferencesPictureBox = new System.Windows.Forms.PictureBox();
            this.OriginalPictureBox = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.comboTestFunctionsList = new System.Windows.Forms.ComboBox();
            this.botonLoadImage = new System.Windows.Forms.Button();
            this.fileDialogLoadImage = new System.Windows.Forms.OpenFileDialog();
            this.progressBarImageProcessing = new System.Windows.Forms.ProgressBar();
            this.labelImageProcessing = new System.Windows.Forms.Label();
            this.labelImageBasicInfo = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.OwnPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OpenCVPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DifferencesPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OriginalPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.labelImageBasicInfo);
            this.groupBox1.Controls.Add(this.textEqualImagesResult);
            this.groupBox1.Controls.Add(this.labelEqualImagesResult);
            this.groupBox1.Controls.Add(this.labelDiffResultTitle);
            this.groupBox1.Controls.Add(this.labelOurResultTitle);
            this.groupBox1.Controls.Add(this.labelOpencvResultTitle);
            this.groupBox1.Controls.Add(this.labelOriginalImageTitle);
            this.groupBox1.Controls.Add(this.OwnPictureBox);
            this.groupBox1.Controls.Add(this.OpenCVPictureBox);
            this.groupBox1.Controls.Add(this.DifferencesPictureBox);
            this.groupBox1.Controls.Add(this.OriginalPictureBox);
            this.groupBox1.Location = new System.Drawing.Point(270, 28);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(790, 581);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            // 
            // textEqualImagesResult
            // 
            this.textEqualImagesResult.Enabled = false;
            this.textEqualImagesResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEqualImagesResult.Location = new System.Drawing.Point(412, 542);
            this.textEqualImagesResult.Name = "textEqualImagesResult";
            this.textEqualImagesResult.Size = new System.Drawing.Size(100, 26);
            this.textEqualImagesResult.TabIndex = 19;
            // 
            // labelEqualImagesResult
            // 
            this.labelEqualImagesResult.AutoSize = true;
            this.labelEqualImagesResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEqualImagesResult.Location = new System.Drawing.Point(277, 545);
            this.labelEqualImagesResult.Name = "labelEqualImagesResult";
            this.labelEqualImagesResult.Size = new System.Drawing.Size(131, 20);
            this.labelEqualImagesResult.TabIndex = 18;
            this.labelEqualImagesResult.Text = "Equals images:";
            // 
            // labelDiffResultTitle
            // 
            this.labelDiffResultTitle.AutoSize = true;
            this.labelDiffResultTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDiffResultTitle.Location = new System.Drawing.Point(294, 290);
            this.labelDiffResultTitle.Name = "labelDiffResultTitle";
            this.labelDiffResultTitle.Size = new System.Drawing.Size(211, 20);
            this.labelDiffResultTitle.TabIndex = 17;
            this.labelDiffResultTitle.Text = "Differences between images";
            // 
            // labelOurResultTitle
            // 
            this.labelOurResultTitle.AutoSize = true;
            this.labelOurResultTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelOurResultTitle.Location = new System.Drawing.Point(600, 290);
            this.labelOurResultTitle.Name = "labelOurResultTitle";
            this.labelOurResultTitle.Size = new System.Drawing.Size(90, 20);
            this.labelOurResultTitle.TabIndex = 16;
            this.labelOurResultTitle.Text = "Own Image";
            // 
            // labelOpencvResultTitle
            // 
            this.labelOpencvResultTitle.AutoSize = true;
            this.labelOpencvResultTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelOpencvResultTitle.Location = new System.Drawing.Point(79, 290);
            this.labelOpencvResultTitle.Name = "labelOpencvResultTitle";
            this.labelOpencvResultTitle.Size = new System.Drawing.Size(119, 20);
            this.labelOpencvResultTitle.TabIndex = 15;
            this.labelOpencvResultTitle.Text = "OpenCV Image";
            // 
            // labelOriginalImageTitle
            // 
            this.labelOriginalImageTitle.AutoSize = true;
            this.labelOriginalImageTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelOriginalImageTitle.Location = new System.Drawing.Point(339, 23);
            this.labelOriginalImageTitle.Name = "labelOriginalImageTitle";
            this.labelOriginalImageTitle.Size = new System.Drawing.Size(125, 20);
            this.labelOriginalImageTitle.TabIndex = 14;
            this.labelOriginalImageTitle.Text = "Original Image";
            // 
            // OwnPictureBox
            // 
            this.OwnPictureBox.Location = new System.Drawing.Point(532, 313);
            this.OwnPictureBox.Name = "OwnPictureBox";
            this.OwnPictureBox.Size = new System.Drawing.Size(241, 204);
            this.OwnPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.OwnPictureBox.TabIndex = 13;
            this.OwnPictureBox.TabStop = false;
            // 
            // OpenCVPictureBox
            // 
            this.OpenCVPictureBox.Location = new System.Drawing.Point(19, 313);
            this.OpenCVPictureBox.Name = "OpenCVPictureBox";
            this.OpenCVPictureBox.Size = new System.Drawing.Size(241, 204);
            this.OpenCVPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.OpenCVPictureBox.TabIndex = 12;
            this.OpenCVPictureBox.TabStop = false;
            // 
            // DifferencesPictureBox
            // 
            this.DifferencesPictureBox.Location = new System.Drawing.Point(275, 313);
            this.DifferencesPictureBox.Name = "DifferencesPictureBox";
            this.DifferencesPictureBox.Size = new System.Drawing.Size(241, 204);
            this.DifferencesPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.DifferencesPictureBox.TabIndex = 11;
            this.DifferencesPictureBox.TabStop = false;
            // 
            // OriginalPictureBox
            // 
            this.OriginalPictureBox.Location = new System.Drawing.Point(275, 79);
            this.OriginalPictureBox.Name = "OriginalPictureBox";
            this.OriginalPictureBox.Size = new System.Drawing.Size(241, 204);
            this.OriginalPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.OriginalPictureBox.TabIndex = 10;
            this.OriginalPictureBox.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(61, 139);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(161, 20);
            this.label1.TabIndex = 12;
            this.label1.Text = "Select the method:";
            // 
            // comboTestFunctionsList
            // 
            this.comboTestFunctionsList.FormattingEnabled = true;
            this.comboTestFunctionsList.Location = new System.Drawing.Point(10, 172);
            this.comboTestFunctionsList.Name = "comboTestFunctionsList";
            this.comboTestFunctionsList.Size = new System.Drawing.Size(246, 21);
            this.comboTestFunctionsList.TabIndex = 11;
            this.comboTestFunctionsList.SelectedIndexChanged += new System.EventHandler(this.ComboTestFunctionsListSelectedIndexChanged);
            // 
            // botonLoadImage
            // 
            this.botonLoadImage.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.botonLoadImage.Location = new System.Drawing.Point(12, 42);
            this.botonLoadImage.Name = "botonLoadImage";
            this.botonLoadImage.Size = new System.Drawing.Size(244, 31);
            this.botonLoadImage.TabIndex = 20;
            this.botonLoadImage.Text = "Load image";
            this.botonLoadImage.UseVisualStyleBackColor = true;
            this.botonLoadImage.Click += new System.EventHandler(this.BotonLoadImageClick);
            // 
            // fileDialogLoadImage
            // 
            this.fileDialogLoadImage.FileName = "openFileDialog1";
            // 
            // progressBarImageProcessing
            // 
            this.progressBarImageProcessing.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.progressBarImageProcessing.Location = new System.Drawing.Point(0, 627);
            this.progressBarImageProcessing.Name = "progressBarImageProcessing";
            this.progressBarImageProcessing.Size = new System.Drawing.Size(1079, 29);
            this.progressBarImageProcessing.TabIndex = 20;
            // 
            // labelImageProcessing
            // 
            this.labelImageProcessing.AutoSize = true;
            this.labelImageProcessing.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelImageProcessing.Location = new System.Drawing.Point(12, 607);
            this.labelImageProcessing.Name = "labelImageProcessing";
            this.labelImageProcessing.Size = new System.Drawing.Size(151, 17);
            this.labelImageProcessing.TabIndex = 21;
            this.labelImageProcessing.Text = "Processing image...";
            // 
            // label2
            // 
            this.labelImageBasicInfo.AutoSize = true;
            this.labelImageBasicInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelImageBasicInfo.Location = new System.Drawing.Point(241, 54);
            this.labelImageBasicInfo.Name = "labelImageInfo";
            this.labelImageBasicInfo.Size = new System.Drawing.Size(0, 20);
            this.labelImageBasicInfo.TabIndex = 20;
            // 
            // FormOperatorsTester
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1079, 656);
            this.Controls.Add(this.labelImageProcessing);
            this.Controls.Add(this.progressBarImageProcessing);
            this.Controls.Add(this.botonLoadImage);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboTestFunctionsList);
            this.Name = "FormOperatorsTester";
            this.Text = "Image operations tester";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.OwnPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OpenCVPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DifferencesPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OriginalPictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox textEqualImagesResult;
        private System.Windows.Forms.Label labelEqualImagesResult;
        private System.Windows.Forms.Label labelDiffResultTitle;
        private System.Windows.Forms.Label labelOurResultTitle;
        private System.Windows.Forms.Label labelOpencvResultTitle;
        private System.Windows.Forms.Label labelOriginalImageTitle;
        private System.Windows.Forms.PictureBox OwnPictureBox;
        private System.Windows.Forms.PictureBox OpenCVPictureBox;
        private System.Windows.Forms.PictureBox DifferencesPictureBox;
        private System.Windows.Forms.PictureBox OriginalPictureBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboTestFunctionsList;
        private System.Windows.Forms.Button botonLoadImage;
        private System.Windows.Forms.OpenFileDialog fileDialogLoadImage;
        private System.Windows.Forms.ProgressBar progressBarImageProcessing;
        private System.Windows.Forms.Label labelImageProcessing;
        private System.Windows.Forms.Label labelImageBasicInfo;
    }

}

