﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenCvSharp;

namespace Task5TestProgram
{
    public class ImageVisualisationTools
    {
        private Mat image;

        public ImageVisualisationTools(Mat img)
        {
            image = img;
        }
        public string GetImageCategory()
        {
            string imgCategory;

            if (image.Channels() == 3)
            {
                imgCategory = "All";
            }
            else
            {
                if (image.Type() == MatType.CV_32FC1)
                {
                    return "Grayscale";
                }

                imgCategory = CheckBinaryImage(image) ? "Binary" : "Grayscale";
            }

            return imgCategory;
        }

        public Mat ConvertForVisualition(Mat image)
        {
            Mat dstImage = image.Clone();

            if (image.Type() == MatType.CV_16UC1 || image.Type() == MatType.CV_16UC3)
            {
                image.ConvertTo(dstImage, MatType.CV_8U, 1 / 256.0, 0);
            }
            else if (image.Type() == MatType.CV_32FC1 || image.Type() == MatType.CV_32FC3)
            {
                image.ConvertTo(dstImage, MatType.CV_8U, 255.0, 0);
            }

            return dstImage;
        }

        public bool CheckBinaryImage(Mat img)
        {
            // Get the min and max value of image
            double minVal, maxVal;
            Cv2.MinMaxLoc(img, out minVal, out maxVal);

            // Return true only if the smaller value is 0 and the greater one is, so it means that the image is binary
            return ((minVal == 0) && (maxVal == 1));
        }
    }

}
