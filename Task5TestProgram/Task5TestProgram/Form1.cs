﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TestImageOperators;
using InterfaceImageOperators;
using OpenCvSharp;
using System.Linq.Expressions;
using OpenCvSharp.Extensions;
using System.Reflection;
using Task5CommonImplementation;
using System.IO;

namespace Task5TestProgram
{
    public partial class FormOperatorsTester : Form
    {
        private List<Func<Mat, TestResults>> functions = new List<Func<Mat, TestResults>>();
        private Mat img;
        private Mat imgOriginal;
        private int functionIndex;
        private BackgroundWorker backgroundImageProcess = new BackgroundWorker();
        private IImageOperators operators = new ClassCommonImplementation();
        private ImageVisualisationTools imgVT = null;
        public FormOperatorsTester()
        {
            InitializeComponent();

            // Background worker            
            backgroundImageProcess.DoWork += ProcessImage;
            backgroundImageProcess.RunWorkerCompleted += ProcessImageCompleted;

            // Progress bar properties
            progressBarImageProcessing.Style = ProgressBarStyle.Marquee;
            progressBarImageProcessing.MarqueeAnimationSpeed = 10;
            progressBarImageProcessing.Visible = false;
            labelImageProcessing.Visible = false;
        }

        private void ClearPictureBoxes()
        {
            OpenCVPictureBox.Image = null;
            OwnPictureBox.Image = null;
            DifferencesPictureBox.Image = null;
            textEqualImagesResult.Text = "";
        }

        // Change the controls status 
        private void ChangeControlsStatusToDefaults(bool reset)
        {
            botonLoadImage.Enabled = reset;
            comboTestFunctionsList.Enabled = reset;
            progressBarImageProcessing.Visible = !reset;
            labelImageProcessing.Visible = !reset;
        }

        private void ProcessImage(object sender, DoWorkEventArgs e)
        {
            // Execute test and get results
            e.Result = functions[functionIndex].Invoke(img);
        }

        private void ProcessImageCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            // First, handle the case where an exception was thrown.
            if (e.Error != null)
            {
                MessageBox.Show(e.Error.Message);
            }
            else if (e.Cancelled)
            {
                MessageBox.Show("Cancelled");
            }
            else
            {
                // Finally, handle the case where the operation 
                // succeeded and show test results
                var results = (TestResults)e.Result; textEqualImagesResult.Text = results.EqualImages.ToString();
                OpenCVPictureBox.Image = BitmapConverter.ToBitmap(imgVT.ConvertForVisualition(results.CvMat));
                OwnPictureBox.Image = BitmapConverter.ToBitmap(imgVT.ConvertForVisualition(results.OwnMat));
                DifferencesPictureBox.Image = BitmapConverter.ToBitmap(imgVT.ConvertForVisualition(results.DiffMat)); 
                ChangeControlsStatusToDefaults(true);
            }
        }

        private void ComboTestFunctionsListSelectedIndexChanged(object sender, EventArgs e)
        {
            functionIndex = comboTestFunctionsList.SelectedIndex;

            ChangeControlsStatusToDefaults(false);
            ClearPictureBoxes();

            backgroundImageProcess.RunWorkerAsync();
        }

        private void BotonLoadImageClick(object sender, EventArgs e)
        {
            string filePath;
            TestFunctions testFunctions = new TestFunctions(operators);

            comboTestFunctionsList.Items.Clear();
            fileDialogLoadImage.InitialDirectory = Path.GetFullPath(Path.Combine(Application.StartupPath, "../../Images"));

            if (fileDialogLoadImage.ShowDialog() == DialogResult.OK)
            {
                filePath = fileDialogLoadImage.FileName;

                // Read original image
                imgOriginal = Cv2.ImRead(filePath, ImreadModes.Unchanged);

                // Clone image for analysis
                img = imgOriginal.Clone();

                imgVT = new ImageVisualisationTools(img);

                labelImageBasicInfo.Text = $"Image Type: { img.Type() } Category Image: { imgVT.GetImageCategory() }";
                OriginalPictureBox.Image = BitmapConverter.ToBitmap(imgVT.ConvertForVisualition(img));

                functions.Clear();

                // Get implementation functions using reflection
                foreach (var function in typeof(TestFunctions).GetMethods(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly))
                {
                    // Get category attribute of the test function to show only the image operators funcgions that can be executed on it
                    var operationCategory = (OperationCategory)function.GetCustomAttributes(typeof(OperationCategory), true)[0];
                    // Get string category name
                    string cat = operationCategory.Category;

                    if (cat.Equals(imgVT.GetImageCategory()) || cat.Equals("All"))
                    {
                        var func = Delegate.CreateDelegate(typeof(Func<Mat, TestResults>), testFunctions, function, false);
                        functions.Add((Func<Mat, TestResults>)func);
                        comboTestFunctionsList.Items.Add(function.Name);
                    }
                }

                ClearPictureBoxes();
            }

        }

    }
}
